#!/usr/bin/env sh

set -eu
IFS=$(printf ' \n\t')

git config --global user.email "devex.bot@gmail.com"
git config --global user.name "DevEx Bot"
git remote set-url --push origin "git@gitlab.com:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}"

mkdir -p ~/.ssh
cp "${BOT_SSH_KEY}" ~/.ssh/id_rsa
cp "${SSH_CONFIG}" ~/.ssh/config
chmod -R 0400 ~/.ssh
