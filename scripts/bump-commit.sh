#!/usr/bin/env sh

set -eu
IFS=$(printf ' \n\t')

set -x

# CHECKOUT MAIN BRANCH
RELEASE_BRANCH="main"
if [ -z "$( git branch | grep '^* '${RELEASE_BRANCH}'$' )" ]; then
  # Checkout main
  git branch -d ${RELEASE_BRANCH} || true # local branch might not exist
  git fetch origin ${RELEASE_BRANCH}:${RELEASE_BRANCH}
  git checkout ${RELEASE_BRANCH}
fi

VERSION_FILE="VERSION"
VERSION=$( cat ${VERSION_FILE} )

# CREATE BUMP COMMIT
bump.sh "${VERSION}" > ${VERSION_FILE}
git add ${VERSION_FILE}
git commit -m "Incremented version from ${VERSION} to $( cat ${VERSION_FILE} ) "
git push origin "${RELEASE_BRANCH}"
