#!/usr/bin/env sh

set -eu
IFS=$(printf ' \n\t')

set -x

RELEASE_PREFIX=${1:-}
[ -z "${RELEASE_PREFIX}" ] && exit 1
[ "${RELEASE_PREFIX}" = "rc" ] || [ "${RELEASE_PREFIX}" = "v" ] || exit 1

VERSION_FILE="VERSION"

# RELEASE NAME
VERSION=$( cat ${VERSION_FILE} )
RELEASE="${RELEASE_PREFIX}${VERSION}"
if [ "${RELEASE_PREFIX}" = "rc"  ]; then
  SHORT_SHA=$( git rev-parse --short HEAD )
  RELEASE="${RELEASE}-${SHORT_SHA}"
fi

# CREATE TAG
git tag --annotate "${RELEASE}" --message "Release ${RELEASE}"
git push --tags
